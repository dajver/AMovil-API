# Flow

**1 Sign in (✮)**

**2 Sign up (✮)**

**2.1 Social sign up (✮)**

**3 Profile get (✮)**

**4 Profile update (✮)**

**5 Profile avatar update (✮)**

**6 Main app data (✮)**

**7 Catalog (✮)**

**7.1 Catalog details (✮)**

**8 Item (✮)**

**9 My orders (✮)**

**9.1 My order details**

**10 Checkout items (✮)**

**11 Add credit card (✮)**

**11.1 Edit credit card (✮)**

**11.2 Delete credit card (✮)**

**12 Get credit cards (✮)**

**13  Get support chat phone (✮)**

**14 Put push notifications api key**

**15 Buy Mobile replenishment**

**16 Get prolana credit (✮)**

**17 Get a credit proposal details (✮)**

**18 Get / Refuse a credit proposal (✮)**

**19 Get user bonuses**

**20 Use user bonuses**

**21 Send confirm code for prolana user on email (✮)**

**22 Login user via prolana credit id and code**

**23 Get brands (✮)**

**24 Get operation systems (✮)**

**25 Get wish list**

**26 Create wish list folder**

**27 Add item to wish list folder**

**28 Delete wish list folder**

**29 Delete item from wish list**

**100 Block Errors**


**--**
**Requests with * are done**
**--**


# 1 Sign in *
Endpoint: `POST /v1/user/signin`

**Request:**

* `username` (String)
* `password` (String)

**Example Request:**

```json
{
"username": "boristheblade",
"password": "qwerty"
}

```

**Success Response:**

```json
{
"token": "0sk123n234hj432mn423",
"user_id" : 232
}
```

**Error Response:**

```json
{
"error_message": "User not found in our records."
}
```



# 2 Sign up *
Endpoint: `POST /v1/user/signup`
**Request:**

* `username` (String)
* `email` (String)
* `password` (String)
* `birthday` (Double)
* `google_auth_token` (Optional String)
* `facebook_auth_token` (Optional String)

**Example Request:**

```json
{
"username": "boristheblade",
"email" : "foo@bar.com",
"password": "qwerty",
"birthdate": 287364823764.23423432,
"google_auth_token" : null,
"facebook_auth_token" : null
}

```

**Success Response:**

```json
{
"token": "0sk123n234hj432mn423",
"user_id" : 232
}
```


**Error Response:**

```json
{
"error_message": "Wrong birthdate date"
}
```

# 2.1 Social sign up
Endpoint: `POST /v1/user/socialSignup`
**Request:**

* `authToken` (String)
* `userId` (long)
* `provider` (String) // google_plus or facebook

**Example Request:**

```json
{
"authToken": "23ewouhbkfsdhiyuwefyagyuoi2uhwegiyvhdsibphugiwyevhbfuh",
"userId" : "1233542356546576545",
"provider": "google_plus"
}

```

**Success Response:**

```json
{
"token": "0sk123n234hj432mn423",
"user_id" : 232
}
```


**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong user id",
"error_message": "Wrong provider"
}
```

# 3 Profile get *
Endpoint: `GET /v1/user?id={user_id}`
Authorization: "Bearer {token}"

**Request: not applicable**


**Success Response:**

```json
{
"username": "boristheblade",
"email" : "foo@bar.com",
"birthdate": 287364823764.23423432,
"google_auth_token" : null,
"facebook_auth_token" : null,
"sex":"male",
"brand":"apple",
"operationSystem":"Windows",
"avatar": "http://....",
"bonus": 100.00,
"phone": "161598787665"
}
```


**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 4 Profile update *
Endpoint: `PUT /v1/user?id={user_id}`
Authorization: "Bearer {token}"

**Request:**

* `username` (String)

**Success Response:**

```json
{
"username": "newUserName",
"email" : "foo@bar.com",
"birthdate": 287364823764.23423432,
"google_auth_token" : null,
"facebook_auth_token" : null,
"sex":"male",
"preferences": [{"Brand": "Apple"}, {"Brand": "Apple"}],
"avatar": "http://...."
}
```


**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```


# 5 Profile avatar update *

Endpoint: `POST /v1/user?id={user_id}`
Content Type: "multipart/form-data"
Authorization: "Bearer {token}"

**Request:**

* `avatar` (Binary)

**Success Response:**

```json
{
"username": "BorisTheblade",
"avatar": "New url for avatar"
}
```


**Error Response:**

```json
{
"error_message": "Bad image type"
}
```


# 6 Main app data *
Endpoint: `GET /v1/main`
Authorization: "Bearer {token}"

**Request: not applicable**

**Success Response:**

```json
{
"main_baner":[
{
"id":123,
"image":"http:// ",
"click_url":"http://"
}
],
"news":[
{
"id":324,
"image":"image link",
"title":"title",
"description":"description text"
}
],
"recomended":[
{
"id":324,
"image":"image link",
"title":"title",
"description":"description text",
"price":123
}
]
}
```

# 7 Catalog *
Endpoint: `GET /v1/catalog`
Authorization: "Bearer {token}"

**Request: not applicable**


**Success Response:**

```json
{
"items": [{ 
	"id": 324,
	"icon": "http://icon_url...",
	"title": "Some text for title"
 }, {
 	"id": 324,
    "icon": "http://icon_url...",
    "title": "Some text for title"
 }]
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```


# 7.1 Catalog details *
Endpoint: `GET /v1/catalog/catalogId=324`
Authorization: "Bearer {token}"


**Request: not applicable**


**Success Response:**

```json
{
"catalogName":"Smartphones",
"items": [{ 
	"id": 324,
	"icon": "http://icon_url...",
	"title": "Some text for title",
	"price":123,
	"oldPrice": 2342,
	"credit":"[special]"
 }, {
 	"id": 324,
    "icon": "http://icon_url...",
    "title": "Some text for title",
    "price":123,
    "oldPrice": 2342,
    "credit":"[special]"
 }]
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```


# 8 Item *

Endpoint: `GET /v1/catalog/item?id={item_id}`
Authorization: "Bearer {token}"

**Request: not applicable**


**Success Response:**

```json
{
"id": 123,
"code": 123123,
"name": "Samsung",
"description" : "....",
"price": 12312,
"discount_price": null,
"currency": "USD",
"images": [{"image":"http://asdasd"}],
"special": ["credit"],
"colors": ["red", "purple", "green", "white", "black"],
"memorySize": [8, 16, 32, 64, 128]
}
```

**Error Response:**

```json
{
"error_message": "Bad image type"
}
```

# 9 My Orders *
Endpoint: `GET /v1/myOrdersList`
Authorization: "Bearer {token}"


**Request: not applicable**


**Success Response:**

```json
[
    {
    "status":"pending",
    "date":23234234,
    "details":[
        {
            "id":13145,
            "image":"image link",
            "title":"title",
            "description":"description text",
            "price":123,
            "quantity":5
        },
        {
            "id":13145,
            "image":"image link",
            "title":"title",
            "description":"description text",
            "price":123,
            "quantity":5
        }
    ]
}, {
        "status":"delivered",
        "date":23234234,
        "details":[
            {
                "id":13145,
                "image":"image link",
                "title":"title",
                "description":"description text",
                "price":123,
                "quantity":5
            }
        ]
    }
]
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 9.1 My Order details 
Endpoint: `GET /v1/myOrdersitem?id={item_id}`
Authorization: "Bearer {token}"


**Request: not applicable**


**Success Response:**

```json
[{
"id": 13145,
"deliveryOption":"Standart Shipping", 
"paymentMethod": {
    "cardNumber": "****2345", 
    "cardType": "visa",
    "billingAddress": "Some addres here"
},
"deliveryAddress": "Some address here", 
"totalPrice": 12333,
"currency":"USD",
"orderNumber": 234667,
"date": 553543534,
"details": [{
    "id": 13145,
    "image":"image link", 
    "title": "title", 
    "description": "description text", 
    "price": 123,
    "quantity": 5
},
{
    "id": 13145,
    "image":"image link", 
    "title": "title", 
    "description": "description text", 
    "price": 123,
    "quantity": 5
}]
}]
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```


# 10 Checkout items *
Endpoint: `POST /v1/checkout`
Authorization: "Bearer {token}"


**Request:**

```json
[{
	"id":342,
	"quantity": 4,
	"price": 8927374,
	"color": "red",
	"memorySize": 16
}, { 
	"id":342,
	"quantity": 1,
	"price": 123,
	"color": "black",
	"memorySize": 32
}, { 
	"id":342,
	"quantity": 6,
	"price": 15453,
	"color": "blue",
	"memorySize": 128
}]
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong product id",
"error_message": "Wrong price",
"error_message": "Wrong structure"
}
```

# 11 Add credit card *
(You need to pick any card processing provider - Stripe for example. Card add procedure should be  different. Using third party allows make card processing more simple and safety. For example yoy can verify cards with Stripe servers by token from me, and also process Apple Pay and Android Pay. )

Endpoint: `POST /v1/cards`
Authorization: "Bearer {token}"

**Request:**

```json
{
    "cardid": 123,
    "cardNumber": 13145891011123,
	"firstName": "Name",
	"lastName": "LastName", 
	"cvv": 832, 
	"validTo": "11.2941"
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong card number",
"error_message": "Wrong date",
"error_message": "Wrong cvv",
"error_message": "Empty user name or last name",
"error_message": "Wrong user name or last name"
}
```

# 11.1 Edit credit card *

Endpoint: `PUT /v1/cards`
Authorization: "Bearer {token}"

**Request:**

```json
{
    "cardNumber": 13145891011123,
	"firstName": "Name",
	"lastName": "LastName", 
	"cvv": 832, 
	"validTo": "11.2941"
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong card number",
"error_message": "Wrong date",
"error_message": "Wrong cvv",
"error_message": "Empty user name or last name",
"error_message": "Wrong user name or last name"
}
```

# 11.2 Delete credit card *

Endpoint: `DELETE /v1/cards/{cardId}`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong card id"
}
```

# 12 Get credit cards *
Endpoint: `GET /v1/cards`
Authorization: "Bearer {token}"

**Success Response:**

```json
[{
    "id" : 1,
	"cardNumber": "**********1123",
	"firstName":"Name", 
	"lastName": "LastName", 
	"validTo": "11.2941"
}, 
{
    "id" : 1,
	"cardNumber": "**********1123",
	"firstName":"Name", 
	"lastName": "LastName", 
	"validTo": "11.2941"
}]
```


**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 13  Get support chat phone *
Endpoint: `GET /v1/support/getSupportPhone/`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"supportPhoneNumber": "+188976875767"
}
```


**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 14 Put push notifications api key
Endpoint: `POST /v1/pushApiKey`
Authorization: "Bearer {token}"

**Request:**

```json
{
    "push_key": "9348073-90yt0uwhfgdfuwhiu08heu9biuh39rbihr3uh94furibh9u3ribh39urfh9u3frb3h9ubi3h9rbierf3h9rubf9hu39re",
    "device_type": "ios / android"
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token",
"error_message": "Wrong api key",
"error_message": "Wrong lenght of token"
}
```

# 15 Buy Mobile replenishment
Endpoint: `POST /v1/buyMobileReplanishment`
Authorization: "Bearer {token}"

**Request:**

```json
{
    "phone": 143456789654,
    "amount":34.23,
    "paymentType":"bonuses/card"
    
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong phone number",
"error_message": "Wrong amount",
"error_message": "Wrong type",
"error_message": "Don't have enough bonuses",
"error_message": "Don't have enough money on account"
}
```

# 16 Get prolana credit *
Endpoint: `GET /v1/getProlanaCredit
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"credits":[{
"id":23423,
"purpose":"Iphone X",
"amount":3875,
"currency":"USD",
"term":"24 month",
"startDate":1523007581,
"endDate":1523007581,
"totalPaid":450,
"loanBalance":3425,
"punctualPayment":150,
"creditPercent":0,
"creditPenalty":0,
"paymentInformation": [{
      "id": 4535,
      "date":1523007581,
      "paymentAmount":150,
      "creditAmount":150,
      "comission":0
    },
    {
     "id": 4535,
     "date":1523007581,
     "paymentAmount":150,
     "creditAmount":150,
     "comission":0
    },
    {
     "id": 4535,
     "date":1523007581,
     "paymentAmount":150,
     "creditAmount":150,
     "comission":0
    }]
},
{
"id": 42222,
"purpose":"Samsund s9",
"amount":3875,
"currency":"USD",
"term":"48 month",
"startDate":1523007581,
"endDate":1523007581,
"totalPaid":450,
"loanBalance":3425,
"punctualPayment":150,
"creditPercent":0,
"creditPenalty":0,
"paymentInformation": [{
      "id": 4535,
      "date":1523007581,
      "paymentAmount":150,
      "creditAmount":150,
      "comission":0
    },
    {
     "id": 4535,
     "date":1523007581,
     "paymentAmount":150,
     "creditAmount":150,
     "comission":0
    },
    {
     "id": 4535,
     "date":1523007581,
     "paymentAmount":150,
     "creditAmount":150,
     "comission":0
    }]
}],
"proposals":[{
    "id": 222345,
    "purpose":"Xiaomi A9 Plus",
    "amount":9874,
    "currency":"USD",
    "term":"12 month",
    "startDate":1523007581,
    "endDate":1523007581,
    "proposalType":"device"
},
{
    "id": 11456,
    "purpose":"Meizu A5",
    "amount":1111,
    "currency":"USD",
    "term":"24 month",
    "startDate":1523007581,
    "endDate":1523007581,
    "proposalType":"device"
},
{
     "id": 21123,
     "amount":10000,
     "currency":"USD",
     "term":"12 month",
     "startDate":1523007581,
     "endDate":1523007581,
     "proposalType":"moneyCash"
   }]
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 17 Get a credit proposal details *
Endpoint: `GET /v1/getProlanaCredit/proposalDetails?creditId={id}`
Authorization: "Bearer {token}"

**Request:**

```json
{
    "id": 11456,
    "purpose":"Meizu A5",
    "amount":1111,
    "currency":"USD",
    "term":"24 month",
    "startDate":1523007581,
    "endDate":1523007581,
    "proposalType":"device"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 18 Refuse a credit proposal *
Endpoint: `POST /v1/actionsProlanaCredit`
Authorization: "Bearer {token}"

**Request:**

```json
{
	"action": "refuse",
	"action": "agree"
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 19 Get user bonuses

Endpoint: `GET /v1/getUserBonuses`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
    "bonusesCount":500,
    "available": true
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 20 Use user bonuses

Endpoint: `POST /v1/useUserBonuses`
Authorization: "Bearer {token}"

**Request:**

```json
{
	"totalPrice": 19999,
	"currency": "USD",
	"items": [{
    	"id":342,
    	"quantity": 4,
    	"price": 8927374
    }, { 
    	"id":342,
    	"quantity": 1,
    	"price": 123
    }, { 
    	"id":342,
    	"quantity": 6,
    	"price": 15453
    }]
}
```

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
"error_message": "Not enough bonuses"
"error_message": "This bonuses unavailable"
}
```

# 21 Send confirm code for prolana user on email *
 
Endpoint: `POST /v1/getConfirmCode/{prolanaCreditId}`

**Success Response:**

```json
{
"status": "Confirmation code was sent to your email"
}
```

**Error Response:**

```json
{
"error_message": "Wrong prolana id"
}
```


# 22 Login user via prolana credit id and code

Endpoint: `POST /v1/loginUserToProlana/`

**Request:**

```json
{
	"prolanaCreditId": 1234,
	"code": 1243,
}
```

**Success Response:**

```json
{
    "status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong prolana id"
"error_message": "Wrong confirm code"
}
```

# 23 Get brands *

Endpoint: `GET /v1/getBrandsList`

Authorization: "Bearer {token}"

**Success Response:**

```json
[{"id":"123","brand":"Apple"},{"id":"121","brand":"Samsung"}, {"id":"124","brand":"LG"}]
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 24 Get operation systems *

Endpoint: `GET /v1/getOperationSystemsList`

Authorization: "Bearer {token}"

**Success Response:**

```json
[{"id":"123","os":"iOS"},{"id":"121","os":"Android"}, {"id":"124","os":"Windows"}]
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```


# 25 Get wish list
Endpoint: `GET /v1/wishlist`
Authorization: "Bearer {token}"


**Request: not applicable**


**Success Response:**

```json
[
    {"id" : 1234
    "name": "Phones",
    "items":{
        "id", 13145,
        "image":"image link",
        "title": "title",
        "description": "description text",
        "price": 123,
        "quantity": 5
        }, ...
        }
]
```

# 26 Create wish list folder
Endpoint: `POST /v1/wishlist/`
Authorization: "Bearer {token}"


**Example Request:**

```json
{
"name": "favorite name"
}

```


**Success Response:**

```json
{
"status": "ok"
}
```


**Error Response:**

```json
{
"error_message": "Wrong auth token"
}
```

# 27 Add item to wish list folder
Endpoint: `POST /v1/wishlist/{listid}/item/{itemId}`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
"error_message": "Wrong favorite id"
}
```

# 28 Delete wish list folder
Endpoint: `DELETE /v1/wishlist/{listid}`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
"error_message": "Wrong favorite id"
}
```

# 29 Delete item from wish list
Endpoint: `DELETE /v1/wishlist/{listid}/item/{itemId}`
Authorization: "Bearer {token}"

**Success Response:**

```json
{
"status": "ok"
}
```

**Error Response:**

```json
{
"error_message": "Wrong auth token"
"error_message": "Wrong item id"
}
```

# 100 Block Errors

**_TODO:_**

Needs errors description for each paragraph
___

Example

**Errors:**

| HTTP | CODE                     | Note |
|------|--------------------------|-------------------|--------
| 422  | MISSING\_FB\_TOKEN       | Missing `registration.facebook_token` |
| 422  | INVALID\_FB_TOKEN        | Server can't validate token - expired or issued to other application |
